package is.ru.arnar.midi;

import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Calendar;
import java.util.Date;

import is.ru.arnar.midi.data.AddConcertNetworkWorker;
import is.ru.arnar.midi.data.OnAddConcertComplete;
import is.ru.arnar.midi.domain.Concert;

public class AddActivity extends FragmentActivity implements OnAddConcertComplete {

    private Concert c;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        c = new Concert("","",new Date(),"","","","","");

        Date cDate = c.getDateOfShow();

        Calendar cal = Calendar.getInstance();
        cal.setTime(cDate);
        Button btn_time = (Button) findViewById(R.id.btn_time);
        btn_time.setText(getString(R.string.time_string, cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE)));
        Button btn_date = (Button) findViewById(R.id.btn_date);
        btn_date.setText(getString(R.string.date_string,
                cal.get(Calendar.DATE), cal.get(Calendar.MONTH), cal.get(Calendar.YEAR)));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    public void onCancel(View view) {
        finish();
    }

    public void onSubmit(View view) {
        Boolean validationError = false;
        EditText eventDateName = (EditText) findViewById(R.id.et_eventDateName);
        EditText userGroupName = (EditText) findViewById(R.id.et_userGroupName);
        EditText eventHallName = (EditText) findViewById(R.id.et_eventHallName);
        EditText imageSource   = (EditText) findViewById(R.id.et_imageSource);
        EditText price         = (EditText) findViewById(R.id.et_price);
        EditText description   = (EditText) findViewById(R.id.et_description);

        if (eventDateName.getText().toString().isEmpty()) {
            validationError = true;
            ((TextView) findViewById(R.id.val_eventDateName)).setVisibility(View.VISIBLE);
        } else {
            ((TextView) findViewById(R.id.val_eventDateName)).setVisibility(View.GONE);
        }

        if (eventHallName.getText().toString().isEmpty()) {
            validationError = true;
            ((TextView) findViewById(R.id.val_eventHallName)).setVisibility(View.VISIBLE);
        } else {
            ((TextView) findViewById(R.id.val_eventHallName)).setVisibility(View.GONE);
        }

        if (userGroupName.getText().toString().isEmpty()) {
            validationError = true;
            ((TextView) findViewById(R.id.val_userGroupName)).setVisibility(View.VISIBLE);
        } else {
            ((TextView) findViewById(R.id.val_userGroupName)).setVisibility(View.GONE);
        }

        if (!validationError) {
            c.setEventDateName(eventDateName.getText().toString());
            c.setUserGroupName(userGroupName.getText().toString());
            c.setEventHallName(eventHallName.getText().toString());
            c.setImageSource(imageSource.getText().toString());
            c.setPrice(price.getText().toString());
            c.setDescription(description.getText().toString());
            c.setName("User submitted event");

            AddConcertNetworkWorker worker = new AddConcertNetworkWorker(this);
            worker.execute(c);
        }
    }

    public void onTimeClick(View view) {
        DialogFragment newFragment = new TimePickerFragment();
        newFragment.show(getFragmentManager(), "timePicker");
    }

    public void onTimeSet(int hour, int minute) {
        Date cDate = c.getDateOfShow();

        cDate.setHours(hour);
        cDate.setMinutes(minute);

        Calendar cal = Calendar.getInstance();
        cal.setTime(cDate);
        Button btn_time = (Button) findViewById(R.id.btn_time);
        btn_time.setText(getString(R.string.time_string, cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE)));
    }

    public void onDateClick(View view) {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getFragmentManager(), "datePicker");
    }

    public void onDateSet(int year, int month, int day) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(c.getDateOfShow());
        cal.set(year, month, day);
        c.setDateOfShow(cal.getTime());
        Button btn_date = (Button) findViewById(R.id.btn_date);
        btn_date.setText(getString(R.string.date_string,
                cal.get(Calendar.DATE), cal.get(Calendar.MONTH), cal.get(Calendar.YEAR)));
    }

    @Override
    public void onAddConcertComplete(Concert concert) {
        Intent intent = new Intent(this, DetailActivity.class);
        intent.putExtra("Concert", concert);
        startActivity(intent);
    }
}
