package is.ru.arnar.midi.data;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import is.ru.arnar.midi.domain.Concert;

/**
 * Created by egills on 28.10.15.
 * Android Development 2015.
 * Package: is.ru.arnar.midi.data.
 */
public class ConcertDB {
    public static final String TableConcert = "concerts";
    public static final String[] TableConcertCols = { "_id",
            "dateOfShow", "eventDateName", "eventHallName",
            "imageSource", "name", "userGroupName",
            "description", "price", "image"
    };

    private static final String sqlCreateTable =
            "CREATE TABLE concerts( " +
                    "_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    "dateOfShow TEXT NOT NULL, " +
                    "eventDateName TEXT NOT NULL, " +
                    "eventHallName TEXT, " +
                    "imageSource TEXT, " +
                    "name TEXT, " +
                    "userGroupName TEXT, " +
                    "description TEXT, " +
                    "price TEXT, " +
                    "image BLOB " +
                    ");";

    private static final String sqlDropTable =
            "DROP TABLE IF EXISTS concerts;";

    public static void createTable(SQLiteDatabase db) {
        db.execSQL(sqlCreateTable);
    }

    public static void dropTable(SQLiteDatabase db) {
        db.execSQL(sqlDropTable);
    }

    public static long insert(SQLiteDatabase db, Concert c) {
        ContentValues cv = new ContentValues();

        cv.put(TableConcertCols[1], c.getDateOfShow().toString());
        cv.put(TableConcertCols[2], c.getEventDateName());
        cv.put(TableConcertCols[3], c.getEventHallName());
        cv.put(TableConcertCols[4], c.getImageSource());
        cv.put(TableConcertCols[5], c.getName());
        cv.put(TableConcertCols[6], c.getUserGroupName());
        cv.put(TableConcertCols[7], c.getDescription());
        cv.put(TableConcertCols[8], c.getPrice());
        cv.put(TableConcertCols[9], c.getImage());

        return db.insert(TableConcert, null, cv);
    }

    public static Cursor query(SQLiteDatabase db) {
        return db.query(TableConcert, TableConcertCols,
                null, null, null, null, null);
    }

    public static long update(SQLiteDatabase db, Concert c) {
        ContentValues cv = new ContentValues();

        cv.put(TableConcertCols[1], c.getDateOfShow().toString());
        cv.put(TableConcertCols[2], c.getEventDateName());
        cv.put(TableConcertCols[3], c.getEventHallName());
        cv.put(TableConcertCols[4], c.getImageSource());
        cv.put(TableConcertCols[5], c.getName());
        cv.put(TableConcertCols[6], c.getUserGroupName());
        cv.put(TableConcertCols[7], c.getDescription());
        cv.put(TableConcertCols[8], c.getPrice());
        cv.put(TableConcertCols[9], c.getImage());

        String date = c.getDateOfShow().toString();
        String name = c.getEventDateName();

        // Update the entry using the date and event name to get the correct entry.
        return db.update(TableConcert, cv, TableConcertCols[1] + "= \'" + date + "\'" +
                " AND " + TableConcertCols[2] + "= \'" + name + "\'", null);
    }
}