package is.ru.arnar.midi.data;

import java.io.Serializable;
import java.util.List;

import is.ru.arnar.midi.domain.Concert;

/**
 * Created by egills on 22.10.15.
 * Android Development 2015.
 * Package: is.ru.arnar.midi.data.
 */
public class DataServiceStub implements DataService, Serializable, OnConcertListComplete {
    private OnConcertListComplete listener;

    public DataServiceStub(OnConcertListComplete listener) {
        this.listener = listener;
    }

    public void getConcerts(int page, int days) {
        ConcertListNetworkWorker concertListNetworkWorker = new ConcertListNetworkWorker(this);
        concertListNetworkWorker.execute(page, days);
    }

    @Override
    public void onConcertListComplete(List<Concert> concertList) {
        listener.onConcertListComplete(concertList);
    }
}
