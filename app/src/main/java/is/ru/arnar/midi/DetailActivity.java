package is.ru.arnar.midi;

import android.content.Intent;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.provider.CalendarContract.Events;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import is.ru.arnar.midi.data.ImageWorker;
import is.ru.arnar.midi.domain.Concert;
import is.ru.arnar.midi.helpers.DateHelper;
import is.ru.arnar.midi.helpers.ImageHelper;

public class DetailActivity extends AppCompatActivity {
    private Concert concert;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        concert = (Concert) getIntent().getSerializableExtra("Concert");

        TextView evenDateName = (TextView) findViewById(R.id.eventDateName);
        evenDateName.setText(concert.getEventDateName());

        TextView evenHallName = (TextView) findViewById(R.id.eventHallName);
        evenHallName.setText(concert.getEventHallName());

        TextView dateOfShow = (TextView) findViewById(R.id.dateOfShow);
        dateOfShow.setText(DateHelper.getFormattedDate(concert.getDateOfShow()));

        TextView userGroupName = (TextView) findViewById(R.id.userGroupName);
        userGroupName.setText(concert.getUserGroupName());

        TextView description = (TextView) findViewById(R.id.description);
        description.setText(concert.getDescription());

        TextView price = (TextView) findViewById(R.id.price);
        price.setText(concert.getPrice());

        ImageView imageView = (ImageView) findViewById(R.id.coverImage);

        if (concert.getImage() == null) {
            String url = concert.getImageSource();
            ImageWorker iw = new ImageWorker(imageView, concert);
            iw.execute(url);
        } else {
            byte[] image = concert.getImage();
            imageView.setImageBitmap(ImageHelper.bytesToBitmap(image));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Redirect the user to his calendar
     * with some prefilled in values.
     * @param view Details view
     */
    public void addToCalender(View view) {
        Intent intent = new Intent(Intent.ACTION_INSERT);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        intent.setType("vnd.android.cursor.item/event");

        intent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, concert.getDateOfShow().getTime());
        intent.putExtra(Events.TITLE, concert.getEventDateName());
        intent.putExtra(Events.EVENT_LOCATION, concert.getEventHallName());
        intent.putExtra(Events.DESCRIPTION, "Verð: " + concert.getPrice());

        startActivity(intent);
    }
}
