package is.ru.arnar.midi.domain;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by egills on 15.10.15.
 * Android Development 2015.
 * Package: is.ru.arnar.midi.domain.
 */
public class Concert implements Serializable {
    private String eventDateName;
    private String name;
    private Date dateOfShow;
    private String userGroupName;
    private String eventHallName;
    private String imageSource;
    private String description;
    private String price;
    private byte[] image;

    public Concert(String eventDateName, String name, Date dateOfShow,
                   String userGroupName, String eventHallName, String imageSource,
                   String description, String price) {
        this.eventDateName = eventDateName;
        this.name = name;
        this.dateOfShow = dateOfShow;
        this.userGroupName = userGroupName;
        this.eventHallName = eventHallName;
        this.imageSource = imageSource;
        this.description = description;
        this.price = price;
        this.image = null;
    }

    public String getEventDateName() {
        return eventDateName;
    }

    public void setEventDateName(String eventDateName) {
        this.eventDateName = eventDateName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDateOfShow() {
        return dateOfShow;
    }

    public void setDateOfShow(Date dateOfShow) {
        this.dateOfShow = dateOfShow;
    }

    public String getUserGroupName() {
        return userGroupName;
    }

    public void setUserGroupName(String userGroupName) {
        this.userGroupName = userGroupName;
    }

    public String getEventHallName() {
        return eventHallName;
    }

    public void setEventHallName(String eventHallName) {
        this.eventHallName = eventHallName;
    }

    public String getImageSource() {
        return imageSource;
    }

    public void setImageSource(String imageSource) {
        this.imageSource = imageSource;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Concert{" +
                "eventDateName='" + eventDateName + '\'' +
                ", name='" + name + '\'' +
                ", dateOfShow=" + dateOfShow +
                ", userGroupName='" + userGroupName + '\'' +
                ", eventHallName='" + eventHallName + '\'' +
                ", imageSource='" + imageSource + '\'' +
                ", description='" + description + '\'' +
                ", price='" + price + '\'' +
                '}';
    }

    public String toJSON() {
        return "{" +
                "\"eventDateName\":\"" + eventDateName + "\"," +
                "\"name\":\"" + name + "\"," +
                "\"dateOfShow\":\"" + dateOfShow + "\"," +
                "\"userGroupName\":\"" + userGroupName + "\"," +
                "\"eventHallName\":\"" + eventHallName + "\"," +
                "\"imageSource\":\"" + imageSource + "\"," +
                "\"description\":\"" + description + "\"," +
                "\"price\":\"" + price + "\"" +
                "}";
    }
}
