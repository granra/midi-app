package is.ru.arnar.midi.data;

/**
 * Created by egills on 22.10.15.
 * Android Development 2015.
 * Package: is.ru.arnar.midi.data.
 */
public interface DataService {
    void getConcerts(int page, int days);
}
