package is.ru.arnar.midi.data;

import android.os.AsyncTask;

import org.json.JSONException;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import is.ru.arnar.midi.domain.Concert;

/**
 * Created by arnar on 10/29/15.
 */
public class ConcertListNetworkWorker extends AsyncTask<Integer, Void, List<Concert>> {
    private OnConcertListComplete listener;

    public ConcertListNetworkWorker(OnConcertListComplete listener) {
        this.listener = listener;
    }

    @Override
    protected List<Concert> doInBackground(Integer... params) {
        HttpURLConnection conn = null;
        List<Concert> concerts = null;
        String jsonString = "{}";
        try {
            URL url;
            if (params[1] == 7 || params[1] == 14) {
                url = new URL("http://188.166.45.210/api/events?page=" + Integer.toString(params[0]) + "&days=" + Integer.toString(params[1]));
            } else {
                url = new URL("http://188.166.45.210/api/events?page=" + Integer.toString(params[0]));
            }
            conn = (HttpURLConnection) url.openConnection();

            InputStream in = new BufferedInputStream(conn.getInputStream());
            jsonString = readStream(in);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            assert conn != null;
            conn.disconnect();
        }

        try {
            concerts = ConcertParser.parseConcerts(jsonString);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return concerts;
    }

    /**
     * Reads the data from the network response
     * and creates a string from it.
     *
     * @param is Input stream from the website.
     * @return JSON data string.
     */
    private String readStream(InputStream is) {
        try {
            ByteArrayOutputStream bo = new ByteArrayOutputStream();
            int i = is.read();
            while (i != -1) {
                bo.write(i);
                i = is.read();
            }
            return bo.toString();
        } catch (IOException e) {
            return "";
        }
    }

    @Override
    protected void onPostExecute(List<Concert> concerts) {
        listener.onConcertListComplete(concerts);
    }
}
