package is.ru.arnar.midi;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

import is.ru.arnar.midi.data.ConcertDB;
import is.ru.arnar.midi.data.DataServiceStub;
import is.ru.arnar.midi.data.DbWorker;
import is.ru.arnar.midi.data.OnConcertListComplete;
import is.ru.arnar.midi.domain.Concert;
import is.ru.arnar.midi.helpers.DbHelper;
import is.ru.arnar.midi.helpers.InfiniteScrollListener;

public class MainActivity extends AppCompatActivity implements OnItemSelectedListener, SwipeRefreshLayout.OnRefreshListener, OnConcertListComplete {
    private ConcertAdapter adapter;
    private ListView mListView;
    private List<Concert> concerts;
    private DataServiceStub data;
    private SwipeRefreshLayout swipeRefreshLayout;
    private String selectedFilter = "This week";
    private boolean loadingMore;
    private int currentSelection;
    private SQLiteDatabase mDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        DbHelper mDbHelper = new DbHelper(getApplicationContext());
        mDb = mDbHelper.getWritableDatabase();
        ConcertDB.dropTable(mDb);
        ConcertDB.createTable(mDb);

        /* load all data */
        data = new DataServiceStub(this);

        /* Since filter selected is fired right away on app start
         * we fill the concerts array there */
        concerts = new ArrayList<>();

        mListView = (ListView) findViewById(R.id.concerts);
        adapter = new ConcertAdapter(this, concerts);
        mListView.setAdapter(adapter);

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                Concert c = concerts.get(arg2);
                viewConcert(c);
            }
        });

        /* Setting up the filter dropdown */
        Spinner spinner = (Spinner) findViewById(R.id.concertFilter);
        spinner.setOnItemSelectedListener(this);

        ArrayList<String> filters = new ArrayList<>();
        filters.add("This week");
        filters.add("Next week");
        filters.add("All");

        // Creating adapter for spinner
        ArrayAdapter<String> filterAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, filters);

        // Drop down layout style
        filterAdapter.setDropDownViewResource(android.R.layout.simple_list_item_activated_1);
        spinner.setAdapter(filterAdapter);

        /* Setting the swipe refresh layout */
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
        swipeRefreshLayout.setOnRefreshListener(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_add) {
            Intent intent = new Intent(this, AddActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void viewConcert(Concert c) {
        Intent intent = new Intent(this, DetailActivity.class);
        intent.putExtra("Concert", c);
        startActivity(intent);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
        /* Changing the active filter and refreshing */
        selectedFilter = adapterView.getItemAtPosition(position).toString();
        this.onRefresh();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
        // do nothing
    }

    @Override
    public void onRefresh() {
        /* Calls the network worker for new data depending
         * on which filter is active. */
        switch (this.selectedFilter) {
            case "This week":   data.getConcerts(1, 7);
                break;
            case "Next week":   data.getConcerts(1, 14);
                break;
            case "All":         data.getConcerts(1, 0);
                break;
        }
    }

    public void loadMoreConcerts(int page, int selection) {
        /* Calls the network worker to get more pages of data */
        this.loadingMore = true;
        this.currentSelection = selection;
        switch (this.selectedFilter) {
            case "This week":   data.getConcerts(page, 7);
                break;
            case "Next week":   data.getConcerts(page, 14);
                break;
            case "All":         data.getConcerts(page, 0);
                break;
        }
    }

    public void setCustomScrollListener() {
        /* Initiate the lazy loader scroll listener */
        this.mListView.setOnScrollListener(new InfiniteScrollListener(0) {
            @Override
            public void loadMore(int page, int totalItemsCount) {
                loadMoreConcerts(page, totalItemsCount - 4);
            }
        });
    }

    @Override
    public void onConcertListComplete(List<Concert> concertList) {
        /* Once we get more data we update the list of concerts */
        if(loadingMore) {
            this.concerts.addAll(concertList);
            this.adapter.setValues(this.concerts);
            mListView.setAdapter(adapter);
            adapter.notifyDataSetChanged();
            mListView.setSelection(this.currentSelection);
            this.loadingMore = false;
        } else {
            this.concerts = concertList;
            this.adapter = new ConcertAdapter(this, this.concerts);
            mListView.setAdapter(adapter);
            adapter.notifyDataSetChanged();
            setCustomScrollListener();
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        DbWorker dw = new DbWorker(getApplicationContext(), concerts);
        dw.execute();
    }
}
