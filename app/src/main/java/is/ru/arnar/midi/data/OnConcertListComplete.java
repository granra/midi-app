package is.ru.arnar.midi.data;

import java.util.List;

import is.ru.arnar.midi.domain.Concert;

/**
 * Created by arnar on 10/29/15.
 */
public interface OnConcertListComplete {
    void onConcertListComplete(List<Concert> concertList);
}
