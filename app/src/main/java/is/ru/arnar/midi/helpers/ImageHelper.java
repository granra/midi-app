package is.ru.arnar.midi.helpers;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.ByteArrayOutputStream;

/**
 * Created by egills on 30.10.15.
 * Android Development 2015.
 * Package: is.ru.arnar.midi.helpers.
 */

/**
 * Helper class to assist with
 * converting images to byte
 * array and vice versa.
 */
public class ImageHelper {
    public static byte[] bitmapToBytes(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        return stream.toByteArray();
    }

    public static Bitmap bytesToBitmap(byte[] bytes) {
        return BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
    }
}
