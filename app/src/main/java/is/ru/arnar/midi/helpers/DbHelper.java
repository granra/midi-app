package is.ru.arnar.midi.helpers;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import is.ru.arnar.midi.data.ConcertDB;

/**
 * Created by egills on 28.10.15.
 * Android Development 2015.
 * Package: is.ru.arnar.midi.data.
 */
public class DbHelper extends SQLiteOpenHelper {
    public static final String DB_NAME = "CONCERTS_DB";
    public static final int DB_VERSION = 3;

    public DbHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        ConcertDB.createTable(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        ConcertDB.dropTable(db);
        onCreate(db);
    }
}