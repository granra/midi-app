package is.ru.arnar.midi.data;

import android.os.AsyncTask;

import org.json.JSONException;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import is.ru.arnar.midi.domain.Concert;

/**
 * Created by arnar on 10/29/15.
 */
public class AddConcertNetworkWorker extends AsyncTask<Concert, Void, Concert> {
    private OnAddConcertComplete listener;

    public AddConcertNetworkWorker(OnAddConcertComplete listener) {
        this.listener = listener;
    }

    @Override
    protected Concert doInBackground(Concert... params) {
        Concert c = null;
        try {
            URL url = new URL("http://188.166.45.210/api/events");
            HttpURLConnection httpcon = (HttpURLConnection) url.openConnection();
            httpcon.setDoOutput(true);
            httpcon.setRequestProperty("Content-Type", "application/json");
            httpcon.setRequestProperty("Accept", "application/json");
            httpcon.setRequestMethod("POST");
            httpcon.connect();

            //Write
            OutputStream os = httpcon.getOutputStream();
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
            writer.write(params[0].toJSON());
            writer.close();
            os.close();

            //Read
            BufferedReader br = new BufferedReader(new InputStreamReader(httpcon.getInputStream(), "UTF-8"));

            String line;
            StringBuilder sb = new StringBuilder();

            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            br.close();
            String result = sb.toString();

            c = ConcertParser.parseConcert(result);
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return c;
    }

    @Override
    protected void onPostExecute(Concert concert) {
        listener.onAddConcertComplete(concert);
    }
}
