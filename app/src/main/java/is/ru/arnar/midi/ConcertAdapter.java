package is.ru.arnar.midi;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import is.ru.arnar.midi.data.ImageWorker;
import is.ru.arnar.midi.domain.Concert;
import is.ru.arnar.midi.helpers.DateHelper;
import is.ru.arnar.midi.helpers.ImageHelper;

/**
 * Created by egills on 22.10.15.
 * Android Development 2015.
 * Package: is.ru.arnar.midi.
 */
public class ConcertAdapter extends ArrayAdapter<Concert> {
    private final Context context;
    private List<Concert> values;

    public List<Concert> getValues() {
        return values;
    }

    public void setValues(List<Concert> values) {
        this.values = values;
    }

    public ConcertAdapter(Context context, List<Concert> objects) {
        super(context, -1, objects);
        this.context = context;
        this.values = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater =
                (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.row_layout, parent, false);

        final ImageView imageView = (ImageView) rowView.findViewById(R.id.row_image);

        /* Setting the event name fx: Rokktónleikar á gauknum */
        TextView eventDateName = (TextView) rowView.findViewById(R.id.eventDateName);
        eventDateName.setText(values.get(position).getEventDateName());

        /* Setting the name (or type) of the event, fx: Tónleikar/Aukatónleikar */
        TextView name = (TextView) rowView.findViewById(R.id.name);
        name.setText(values.get(position).getName());

        /* Setting the location of the event */
        TextView eventHallName = (TextView) rowView.findViewById(R.id.eventHallName);
        eventHallName.setText(values.get(position).getEventHallName());

        /* Setting the date of the show, format example: Friday 30. at 18:30 */
        TextView dateOfShow = (TextView) rowView.findViewById(R.id.dateOfShow);
        dateOfShow.setText(DateHelper.getFormattedDate(values.get(position).getDateOfShow()));

        if (values.get(position).getImage() == null) {
            String url = values.get(position).getImageSource();
            ImageWorker iw = new ImageWorker(imageView, values.get(position));
            iw.execute(url);
        } else {
            byte[] image = values.get(position).getImage();
            imageView.setImageBitmap(ImageHelper.bytesToBitmap(image));
        }

        return rowView;
    }
}
