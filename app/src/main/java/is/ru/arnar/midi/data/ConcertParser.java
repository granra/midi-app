package is.ru.arnar.midi.data;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import is.ru.arnar.midi.domain.Concert;

/**
 * Created by arnar on 10/29/15.
 */
public final class ConcertParser {
    private ConcertParser() {}

    public static List<Concert> parseConcerts(String json) throws JSONException {
        List<Concert> lst = new ArrayList<>();

        JSONObject obj = new JSONObject(json);
        JSONArray items = obj.getJSONArray("results");

        for (int i = 0; i < items.length(); ++i) {
            JSONObject item = items.getJSONObject(i);
            String eventDateName = item.getString("eventDateName");
            String name = item.getString("name");
            Date dateOfShow = parseDateString(item.getString("dateOfShow"));
            String userGroupName = item.getString("userGroupName");
            String eventHallName = item.getString("eventHallName");
            String imageSource = item.getString("imageSource");
            String price = item.getString("price");
            String description = item.getString("description");

            lst.add(new Concert(eventDateName, name, dateOfShow,
                    userGroupName, eventHallName, imageSource, description, price));
        }

        return lst;
    }

    public static Concert parseConcert(String json) throws JSONException {
        JSONObject jsonObject = new JSONObject(json);

        String eventDateName = jsonObject.getString("eventDateName");
        String name = jsonObject.getString("name");
        Date dateOfShow = parseDateString(jsonObject.getString("dateOfShow"));
        String userGroupName = jsonObject.getString("userGroupName");
        String eventHallName = jsonObject.getString("eventHallName");
        String imageSource = jsonObject.getString("imageSource");
        String price = jsonObject.getString("price");
        String description = jsonObject.getString("description");

        return new Concert(eventDateName, name, dateOfShow,
                userGroupName, eventHallName, imageSource, description, price);
    }

    private static Date parseDateString(String dateString) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        try {
            return format.parse(dateString);
        } catch (ParseException e) {
            return null;
        }
    }
}
