package is.ru.arnar.midi.data;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;

import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.URL;

import is.ru.arnar.midi.domain.Concert;
import is.ru.arnar.midi.helpers.ImageHelper;

/**
 * Created by egills on 23.10.15.
 * Android Development 2015.
 * Package: is.ru.arnar.midi.data.
 */
public class ImageWorker extends AsyncTask<String, Void, Bitmap> {
    private final WeakReference<ImageView> imageViewWeakReference;
    private final WeakReference<Concert> concertWeakReference;

    public ImageWorker(ImageView imageView, Concert concert) {
        this.imageViewWeakReference = new WeakReference<>(imageView);
        this.concertWeakReference = new WeakReference<>(concert);
    }

    @Override
    protected Bitmap doInBackground(String... params) {
        URL url;
        InputStream content = null;

        try {
            url = new URL(params[0]);
            content = (InputStream) url.getContent();
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }

        return BitmapFactory.decodeStream(content);
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        if (isCancelled()) {
            bitmap = null;
        }

        ImageView imageView = imageViewWeakReference.get();
        Concert concert = concertWeakReference.get();
        if (imageView != null && bitmap != null) {
            imageView.setImageBitmap(bitmap);
        }

        if (concert != null && bitmap != null) {
            // Cache the image bytes in the object if it is not already.
            concert.setImage(ImageHelper.bitmapToBytes(bitmap));
        }
    }
}
