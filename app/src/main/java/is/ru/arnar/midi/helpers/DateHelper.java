package is.ru.arnar.midi.helpers;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by egills on 30.10.15.
 * Android Development 2015.
 * Package: is.ru.arnar.midi.helpers.
 */
public class DateHelper {
    /**
     * Returns the proper date string.
     * Example: "Saturday, 31st Oct at 15:00"
     *
     * @param date Date object
     * @return Formatted date string
     */
    public static String getFormattedDate(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("cccc d'" +
                dayNumberSuffix(date) + " 'MMM' at' HH:mm");
        return dateFormat.format(date);
    }

    /**
     * Helper function that takes care of
     * arranging the day into the required
     * string.
     * Example: 3 -> 3rd
     *
     * @param date Number of the day in the month
     * @return Day suffix
     */
    private static String dayNumberSuffix(Date date) {
        SimpleDateFormat formatDayOfMonth = new SimpleDateFormat("d");
        int n = Integer.parseInt(formatDayOfMonth.format(date));

        if (n >= 11 && n <= 13) {
            return "th";
        }

        switch (n % 10) {
            case 1:
                return "st";
            case 2:
                return "nd";
            case 3:
                return "rd";
            default:
                return "th";
        }
    }
}
