package is.ru.arnar.midi.data;

import is.ru.arnar.midi.domain.Concert;

/**
 * Created by arnar on 10/29/15.
 */
public interface OnAddConcertComplete {
    void onAddConcertComplete(Concert concert);
}
