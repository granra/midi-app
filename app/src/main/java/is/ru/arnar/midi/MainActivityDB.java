package is.ru.arnar.midi;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import is.ru.arnar.midi.data.ConcertDB;
import is.ru.arnar.midi.domain.Concert;
import is.ru.arnar.midi.helpers.DbHelper;

/**
 * Created by egills on 30.10.15.
 * Android Development 2015.
 * Package: is.ru.arnar.midi.
 */
public class MainActivityDB extends AppCompatActivity {
    private ConcertAdapter adapter;
    private ListView mListView;
    private List<Concert> concerts;

    private DbHelper mDbHelper;
    private SQLiteDatabase mDb;
    private Cursor mCursor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maindb);

        concerts = new ArrayList<>();

        mDbHelper = new DbHelper(getApplicationContext());
        mDb = mDbHelper.getReadableDatabase();

        mCursor = ConcertDB.query(mDb);
        startManagingCursor(mCursor);

        // DB Columns:
        // -----------
        // 0: "_id"
        // 1: "dateOfShow"
        // 2: "eventDateName"
        // 3: "eventHallName"
        // 4: "imageSource"
        // 5: "name"
        // 6: "userGroupName"
        // 7: "description"
        // 8: "price"
        // 9: "image"

        for (mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor.moveToNext()) {
            Date dateOfShow = new Date(mCursor.getString(1));
            String eventDateName = mCursor.getString(2);
            String eventHallName = mCursor.getString(3);
            String imageSource = mCursor.getString(4);
            String name = mCursor.getString(5);
            String userGroupName = mCursor.getString(6);
            String description = mCursor.getString(7);
            String price = mCursor.getString(8);
            byte[] image = mCursor.getBlob(9);

            Concert c = new Concert(eventDateName, name, dateOfShow, userGroupName,
                    eventHallName, imageSource, description, price);
            c.setImage(image);
            concerts.add(c);
        }

        mListView = (ListView) findViewById(R.id.concerts);
        adapter = new ConcertAdapter(this, concerts);

        mListView.setAdapter(adapter);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                Concert c = concerts.get(arg2);
                viewConcert(c);
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_add) {
            Toast.makeText(MainActivityDB.this, "No network connection", Toast.LENGTH_SHORT).show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void viewConcert(Concert c) {
        Intent intent = new Intent(this, DetailActivity.class);
        intent.putExtra("Concert", c);
        startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mDb.close();
    }
}
