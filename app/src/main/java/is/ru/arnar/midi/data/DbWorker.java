package is.ru.arnar.midi.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;

import java.util.List;

import is.ru.arnar.midi.domain.Concert;
import is.ru.arnar.midi.helpers.DbHelper;

/**
 * Created by egills on 31.10.15.
 * Android Development 2015.
 * Package: is.ru.arnar.midi.data.
 */
public class DbWorker extends AsyncTask<Void, Void, Void> {
    private final List<Concert> concerts;
    private final SQLiteDatabase mDb;

    public DbWorker(Context context, List<Concert> concerts) {
        this.concerts = concerts;
        this.mDb = new DbHelper(context).getWritableDatabase();
    }

    @Override
    protected Void doInBackground(Void... params) {
        for (Concert c : concerts) {
            ConcertDB.insert(mDb, c);
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        mDb.close();
    }
}
